#!/bin/bash 

NEWLOC=`curl -L -k "https://snootles.dsc.umich.edu/packages/blackmagicdesktopvideo/" -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' 2>/dev/null | /usr/local/bin/htmlq -a href a | grep .dmg | sed 's@\.\/@@'`

if [ "x${NEWLOC}" != "x" ]; then    
	echo "https://snootles.dsc.umich.edu/packages/blackmagicdesktopvideo/${NEWLOC}"
fi